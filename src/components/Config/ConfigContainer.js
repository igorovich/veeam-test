import {Config} from "./Config"
import {connect} from "react-redux"
import { selectFormJson } from "../../store/selectors/appSelector"
import { updateJSON } from "../../store/actions/appActions"

const mapStateToProps = (state) => ({
    formJSON: selectFormJson(state),
})

const mapDispatchToProps = ({
    updateJSON,
})

export const ConfigContainer = connect(mapStateToProps, mapDispatchToProps)(Config)