import React, { useEffect, useRef } from 'react'
import "./Config.css"

export const Config = ({
        formJSON,
        updateJSON
    }) => {
    const onSubmit = (e) => {
        e.preventDefault();
        updateJSON(e.target[0].value)
    }
    const inputEl = useRef(null);
    useEffect(() => {
        inputEl.current.value = JSON.stringify(formJSON)
    }, [formJSON])

    return(
        <form onSubmit={(onSubmit)}>
            < textarea className = "configInput"
                ref={inputEl}
            /> 
            <div>
                <button className="applyButton">Apply</button>
            </div>
        </form>
    );
}
