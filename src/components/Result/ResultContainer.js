import { Result } from "./Result"
import { connect } from "react-redux"
import { selectFormJson } from "../../store/selectors/appSelector"
import { updatePartialJSON } from "../../store/actions/appActions"

const mapStateToProps = (state) => ({
    formJSON: selectFormJson(state),
});
const mapDispatchToProps = ({
    updatePartialJSON,
})
export const ResultContainer = connect(mapStateToProps, mapDispatchToProps)(Result)