import React from 'react'
import "./Result"
import "./Result.css"

const getElementType = ({ type, value, checkedHandler, changeHandler, itemKey}) => {
    switch (type) {
        case "number":
            return <input id={itemKey} onChange={changeHandler} type="number" value={value}/>
        case "text":
            return <input id={itemKey} onChange={changeHandler} value={value} />
        case "textarea":
            return <textarea id={itemKey} onChange={changeHandler} value={value} />
        case "checkbox":
            return <input id={itemKey} onChange={checkedHandler} type="checkbox" checked={JSON.parse(value)} />
        case "date":
            return <input id={itemKey} onChange={changeHandler} type="date" value={value} />
        case "radio":
            return <input id={itemKey} onChange={checkedHandler} type="radio" checked={JSON.parse(value)} />
        default:
            return null;
    }
}

const ResultItem = ({ type, value, label, checkedHandler, changeHandler, itemKey}) => {
    const element = getElementType({ type, value, checkedHandler, changeHandler, itemKey})
    return <div className="row">
        <div className="col-25">
            <label>{label}</label>
        </div>
        <div className="col-75">
            {element}
        </div>
    </div>
}
const ResultButton = (props) => {
    return <div className="result-buttons">
        <button>{props.button}</button>
    </div>

}

export const Result = ({ formJSON, updatePartialJSON}) => {
    const onChangeHandler = (e) => {
        const value = e.target.value;        
        const id = e.target.getAttribute('id');
        updatePartialJSON(value, id)
    }
    const onChangeCheckedHandler = (e) => {
        const value = e.target.checked;
        const id = e.target.getAttribute('id');
        updatePartialJSON(value, id)
    }
   
    return(
        <div className='container'>
            <h1>{formJSON.header}</h1>
            {formJSON.items.map((item, key) => <ResultItem {...item} key={key} itemKey={key} checkedHandler={onChangeCheckedHandler} changeHandler={onChangeHandler}  />)}
            {formJSON.buttons.map((button, key) => <ResultButton button={button} key={key}/>)}
        </div>
    )    
}