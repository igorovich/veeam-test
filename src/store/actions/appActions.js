export const updateJSON = (text) => async (dispatch) => {
    try {
        const parsedString = JSON.parse(text);
            dispatch({
                type: "UPDATE_JSON",
                payload: parsedString
            })
    } catch (err) {
        console.error(err);
        alert("error")
    }
};
export const updatePartialJSON = (value, id) => async (dispatch) => {
    const payloadObject = {value: value, id: id};
    dispatch({
        type: "UPDATE_PARTIAL_JSON",
        payload: payloadObject,
    })
}