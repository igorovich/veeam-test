import {
    selectFormJson
} from "../appSelector"

describe("appSelector", () => {
    it("should select fromJSON", () => {
        const json = {
            app: {
                test: "aa",
                formJSON: {
                    x: "y"
                }
            }
        };
        expect(selectFormJson(json)).toEqual({x: "y"})
    })
})