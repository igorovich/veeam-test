import produce from "immer";
import formJSON from "../../test"


const defaultState = {
    formJSON: formJSON,
};

/* eslint-disable default-case */
/**
 * Redux reducer for routes
 * @param state
 * @param action
 */
const appReducer = (state = defaultState, action) =>
    produce(state, draft => {
        
        const { type, payload = {} } = action;
        switch (type) {
            case "UPDATE_JSON": {
                draft.formJSON = payload;
                break;
            }
            case "UPDATE_PARTIAL_JSON": {
                draft.formJSON.items[payload.id].value = payload.value;
                break;
            }
        }
        return draft
    });

export default appReducer;
