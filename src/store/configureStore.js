import {
    applyMiddleware,
    createStore,
    compose,
} from "redux";
import thunk from "redux-thunk";
import setupReducers from "./reducers/rootReducer";

let composeEnhancers = compose;

/**
 * Redux store export
 * @returns {Store<*, Action> & {dispatch: any}}
 */
export default function configureStore() {

    return createStore(
        setupReducers(),
        // {},
        composeEnhancers(applyMiddleware(thunk))
    );
}
