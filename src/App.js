import React from 'react';
// import logo from './logo.svg';
import './App.css';
import {ConfigContainer} from './components/Config/ConfigContainer'
import {
    ResultContainer
} from './components/Result/ResultContainer'
// import Link from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


const App = () => {
    return(
        <div>
            <Router basename="veeam-test">
                <div className="nav">
                    <div className="nav-links">
                        < div className = "inline-text" >
                            <Link to={'/'}>Config</Link>
                        </div>
                        < div className = "inline-text" >
                            <Link to={'/result'}>Result</Link>
                        </div>
                    </div>
                </div>
                <Switch>
                    <Route exact path="/" component={ConfigContainer} />
                    <Route path="/result" component={ResultContainer} />
                </Switch>
            </Router>
        </div>
    )
};
export default App;