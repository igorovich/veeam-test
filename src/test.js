export default {
    header: "test",
    items: [{
        label: "number",
        type: "number",
        value: "12"           
    },
    {
        label: "text",
        type: "text",
        value: "string text"
    },
    {
        label: "textarea",
        type: "textarea",
        value: "Multi-line \ntextarea"
    },
    {
        label: "checkbox",
        type: "checkbox",
        value: true
    },
    {
        label: "date",
        type: "date",
        value: "2019-09-13"
    },
    {
        label: "radio",
        type: "radio",
        value: "false"
    }
],
    buttons: ["Ok", "Cancel", "Apply", "Back"]
}